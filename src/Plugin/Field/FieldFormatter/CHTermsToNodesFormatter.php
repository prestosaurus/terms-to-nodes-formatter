<?php

namespace Drupal\ch_terms_to_nodes_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin implementation of the 'CH Terms to Nodes' formatter.
 *
 * @FieldFormatter(
 *   id = "ch_terms_to_nodes_formatter",
 *   label = @Translation("Terms to Nodes Formatter"),
 *   description = @Translation("Display the titles of nodes related to the term"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class CHTermsToNodesFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Entity Type Manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'use_commas' => '',
      'show_trailing_comma' => '',
      'content_type_list' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $yesNoOptions = [
      'yes' => 'Yes',
      'no' => 'No',
    ];

    $element = [];

    $element['use_commas'] = [
      '#title' => $this->t('Separate by commas'),
      '#description' => $this->t('Places a comma after each item.'),
      '#type' => 'select',
      '#options' => $yesNoOptions,
      '#required' => 'required',
      '#default_value' => $this->getSetting('use_commas') ?: 'yes',
    ];

    $element['show_trailing_comma'] = [
      '#title' => $this->t('Show a trailing comma? ("Separate by commas" must be set to "Yes")'),
      '#description' => $this->t('Show or hide comma for the last item. A single reference item counts as the last item.'),
      '#type' => 'select',
      '#options' => $yesNoOptions,
      '#required' => 'required',
      '#default_value' => $this->getSetting('show_trailing_comma') ?: 'no',
    ];

    $element['content_type_list'] = [
      '#title' => $this->t('Allowed content type'),
      '#description' => $this->t('Defines which content type we will allow to reference.'),
      '#type' => 'select',
      '#options' => get_node_content_types(),
      '#required' => 'required',
      '#default_value' => $this->getSetting('content_type_list') ?: '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];
    // $settingsList = [];
    $settings = $this->getSettings();

    $summary[] = $this->t('Separate With Commas?@space', ['@space' => ' ']) . $settings['use_commas'];
    $summary[] = $this->t('Show Trailing Comma?@space', ['@space' => ' ']) . $settings['show_trailing_comma'];
    $summary[] = $this->t('Allowed Content Type:@space', ['@space' => ' ']) . $settings['content_type_list'];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Verify langcode is set.
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    $node = $items->getEntity();

    $no_additional_topics = $node->get('field_additional_topic_cat')->isEmpty();

    // Get the field name our formatter is applied to.
    $fieldName = $items->getName();

    // Get our form settings.
    $useComma = $this->getSetting('use_commas');
    $showTrailingComma = $this->getSetting('show_trailing_comma');
    $contentTypeList = $this->getSetting('content_type_list');

    // Instatiate variables.
    $elements = [];
    $index = 0;
    $addComma = "";
    $theme = 'ch_terms_to_nodes';
    $nidValues = [];

    // Loop through our field entities.
    foreach ($this->getEntitiesToView($items, $langcode) as $item) {

      // Get our vocabulary type.
      $vocabularyType = $item->bundle();

      // Query our NID's matching criteria.
      $query = $this->entityTypeManager->getStorage('node')->getQuery();
      $result = $query
        ->condition('type', $contentTypeList)
        ->condition('field_primary_topic.entity.tid', $item->id(), '=')
        ->condition('status', 1)
        ->sort('title', 'ASC')
        ->execute();

      if ($result) {
        $nidValues[] = array_values($result)[0];
      }

    }

    // Load our desired nodes.
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nidValues);

    // Count our nodes.
    $node_count = count($nodes);

    // Loop through our nodes.
    foreach ($nodes as $key => $node) {

      // Iterate our index counter.
      $index++;

      // Get our node values.
      $nodeType = $node->bundle();
      $title = $node->title->value;
      $url = $node->toUrl()->toString();

      // Check to add commas.
      if ($useComma === 'yes') {
        $addComma = ", ";

        if ($showTrailingComma === 'no' && $index === $node_count || $no_additional_topics) {
          $addComma = "";
        }
      }

      // Build our elements array.
      $elements[$key] = [
        '#theme' => $theme,
        '#title' => $title,
        '#url' => $url,
        '#nodeType' => $nodeType,
        '#vocabularyType' => $vocabularyType,
        '#fieldname' => $fieldName,
        '#addComma' => $addComma,
      ];

    }
    return $elements;
  }

}
